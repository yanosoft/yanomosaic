
// c headers
#include <unistd.h>

// c++ headers
#include <string>
#include <stdexcept>

// OpenCV headers
#include <opencv2/opencv.hpp>

// local headers
#include "yanoMosaic.h"

void Usage()
{
  std::cout << "./yanoMosaic <options>" << std::endl <<
               "\t-h - Prints this help menu and exits." << std::endl <<
//               "\t-c [directory] - Converts directory of color images to grayscale images. "
//                   "It creates a directory called \"Grayscales\" in the directory, and all images "
//                   "it finds, it converts them to grayscale and writes them to the Grayscales "
//                   "directory. The file names are preserved." << std::endl <<
               "\t-t [directory] - Specifies the source directory where thumbnails already live ("
                   "created with the -p option). This is mutually exclusive with the -p option." <<
                   std::endl <<
               "\t-p [directory] - Specifies a source directory of images which will be converted "
                   "to thumbnails. All images must be in the top directory, it does not perform a "
                   "recursive search. This is mutually exclusive with the -t option." << std::endl <<
               "\t-g [WidthxHeight] - Specifies the grid diemsions to be laid down on sourceImage. "
                   "This is required for the -p option and ignored for the -t option. "
                   "Each grid cell is a mosaic tile. Width and Height must both be > 0 and separated "
                   "with 'x'. There must be no spaces in the string, ie 43x84." << std::endl <<
               "\t-s [sourceImage] - Specifies the source image to be mosaiced." << std::endl <<
               "\t-z [outputPath] - Specifies the output path for the final mosaiced image. "
                   "This is optional, and if ommited, the mosaiced image will simply be created "
                   "in the directory where the source image lives." << std::endl <<
               "\t-d [directory] - The output directory where the thumbnails will be saved when "
                   "the -p option is used. If this is unspecified, then it will be set to the "
                   "current directory." << std::endl <<
               "\t-v Specifies to launch a live video feed rather than mosaic a single image." << std::endl;

  std::cout << std::endl;
}

int main(int argc, char** argv )
{
  int opt;
  bool liveVideoFeed = false;

  YanoMosaic yanoMosaic;

  if (argc == 1)
  {
    Usage();
    return 0;
  }

  try
  {
    while( (opt=getopt(argc, argv, "p:g:s:d:ht:z:v")) != -1)
    {
      switch (opt)
      {
        case 'g':
        {
          yanoMosaic.ConvertGridDimensions(std::string(optarg));
          break;
        }
        case 's':
        {
          yanoMosaic.SetSourceImage(cv::String(optarg));
          break;
        }
        case 'd':
        {
          yanoMosaic.SetThumbnailDestDir(std::string(optarg));
          break;
        }
        case 't':
        {
          yanoMosaic.SetThumbnailSrcDir(std::string(optarg));
          break;
        }
        case 'p':
        {
          yanoMosaic.SetSourceImagesDir(std::string(optarg));
          break;
        }
        case 'z':
        {
          yanoMosaic.SetOutputMosaicImageName(cv::String(optarg));
          break;
        }
        case 'h':
        {
          Usage();
          return 0;
        }
        case 'v':
        {
          liveVideoFeed = true;
          break;
        }
        default:
        {
          std::cerr << "Unknown option " << opt << std::endl;
          return -1;
        }
      }
    }

    if (liveVideoFeed == true)
    {
      // perform live video feed
      yanoMosaic.MosaicLiveVideo();
    }
    else
    {
      yanoMosaic.CreateMosaic();
    }
  }
  catch (const std::invalid_argument& ia)
  {
    std::cerr << "Caught invalid argument exception: " << ia.what() << std::endl;
    Usage();
  }
  catch(const std::runtime_error& rte)
  {
    std::cerr << "Caught runtime error exception: " << rte.what() << std::endl;
    Usage();
  }
  catch (...)
  {
    std::cerr << "Caught unknown exception.. I don't know what happened or how to "
                 "fix this." << std::endl;
    Usage();
  }

  return 0;
}
