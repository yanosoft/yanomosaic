
#ifndef YANO_MOSAIC_H_
#define YANO_MOSAIC_H_

// c++ headers
#include <string>
#include <map>

// opencv headers
#include <opencv2/opencv.hpp>

// struct for storing all information we'll need for a picture. We won't need
// name many places, but we will some places, so just use this struct throughout
struct Picture
{
  // name of the picture
  std::string picName;
  // all the picture picData528
  cv::Mat picData;
  // number of times this picture is used
  unsigned numTimesUsed;

  Picture()
    : numTimesUsed(0)
  { }
};

/**********************************************************************************
 * Name: YanoMosaic
 * Description: This class performs a photomosaic transformation in grayscale. The
 *    user supplies a directory of images to use as "thumbnails", calculates the
 *    average intensity of each image, and lays down the closest matching thumbnail
 *    in each cell of the imposed grid. Creating the thumbnails takes some processing
 *    power (and time), so after they are created, the user can specify the directory
 *    where they reside for all future photomosaic transformations.
 * ********************************************************************************/
class YanoMosaic
{
public:

  /**********************************************************************************
   * Name: YanoMosaic
   * Description: constructor
   * ********************************************************************************/
  YanoMosaic();

  /**********************************************************************************
   * Name: YanoMosaic
   * Description: destructor
   * ********************************************************************************/
  ~YanoMosaic();

  /**********************************************************************************
   * Name: ConvertGridDimensions
   * Description: translates the string input grid dimensions to numbers. Must be in
   *    the format AAAxBBB where 'x' separates AAA (the grid width) and BBB (the grid
   *    height)
   * Parameters:
   *    - strGridDims - the string representation of the laydown-grid dimensions. For
   *        example "-g100x100" will lay down a 100 x 100 grid over the source image,
   *        and each cell's intensity will be calculated and matched with the
   *        thumbnail that most closely matches that same intensity
   * ********************************************************************************/
  void ConvertGridDimensions(const std::string& strGridDims);

  /**********************************************************************************
   * Name: SetSourceImage
   * Description: specifies the image that is to be mosaiced
   * Parameters:
   *    - srcImage - the full file path to the image to be mosaiced. This is option is
   *        mandatory.
   * ********************************************************************************/
  void SetSourceImage(const cv::String& srcImage);

  /**********************************************************************************
   * Name: SetSourceImage
   * Description: specifies the image that is to be mosaiced
   * Parameters:
   *    - imgName - the name of the output mosaiced image. This option is optional
   *        (if omitted, this will be set to <imageName>_mos
   * ********************************************************************************/
  inline void SetOutputMosaicImageName(const cv::String& imgName)  { outputMosaicImage_ = imgName; }

  /**********************************************************************************
   * Name: SetThumbnailDestDir
   * Description: sets the directory where created thumbnails will be saved
   * Parameters:
   *    - tnDestDir - sets the thumbnail destination directory, where thumbnails will
   *        be saved.
   * ********************************************************************************/
  void SetThumbnailDestDir(const std::string& tnDestDir);

  /**********************************************************************************
   * Name: SetThumbnailSrcDir
   * Description: sets the directory where the images that will be converted to
   *    thumbnails reside.
   * Parameters:
   *    - tnSrcDir - specifies the directory where already-created thumbnails reside
   * ********************************************************************************/
  void SetThumbnailSrcDir(const std::string& tnSrcDir);

  /**********************************************************************************
   * Name: SetSourceImagesDir
   * Description: sets the directory where images that will be converted to thumbnails
   *    reside
   * Parameters:
   *    - srcImagesDir - The directory where your source images reside. These images
   *        will be converted to grayscale thumbnails and used to stitch together the
   *        final mosaiced image.
   * ********************************************************************************/
  void SetSourceImagesDir(const std::string& srcImagesDir);

  /**********************************************************************************
   * Name: CreateMosaic
   * Description: the function that kicks off the entire conversion process, whether
   *    or not the thumbnails have already been created
   * ********************************************************************************/
  void CreateMosaic();

  /**********************************************************************************
   * Name: MosaicLiveVideo
   * Description: launches a window where live video feed is mosaiced, all in the same
   *    manner that a still image is. So all the thumbnails must be specified/created
   *    just the same as for still images
   * ********************************************************************************/
  void MosaicLiveVideo();

private:

  // name of the thumbnail metadata file
  const std::string THUMBNAIL_INTENSITY_FILE = "ThumbnailIntensities.txt";
  // max pixel intensity as a double
  const double MAX_PIXEL_INTENSITY = 255.0;

  /**********************************************************************************
   * Name: AddTrailingSlash
   * Description: adds a trailing slash ('/') to the end of
   * Parameters:
   *    - dir - the directory path to examine
   * ********************************************************************************/
  std::string AddTrailingSlash(const std::string& dir);

  /**********************************************************************************
   * Name: CalculateCellDimensions
   * Description: calculates the width and height (in pixels) of the cells that
   *    comprise the grid. This is calculated based on the source image resolution
   *    and the grid laydown dimesions.
   * Parameters:
   *    - cellWidth - on return, contains the width of each grid cell
   *    - cellHeight - on return, contains the height of each grid cell
   *    - srcImgMat - the source image, either specified for a single static image
   *        or a frame captured from the video camera. All cell and grid dimensions
   *        are calculated based on this image's dimensions.
   * ********************************************************************************/
  void CalculateCellDimensions(unsigned& cellWidth,
                               unsigned& cellHeight,
                               const cv::Mat& srcImgMat);

  /**********************************************************************************
   * Name: CalculateGridDimensions
   * Description: calulates the grid dimensions based on the cellWidth and cellHeight
   *    read from file
   * Parameters:
   *    - cellWidth - cellWidth read from file
   *    - cellHeight - cellHeight read from file
   *    - srcImgMat - the source image, either specified for a single static image
   *        or a frame captured from the video camera. All cell and grid dimensions
   *        are calculated based on this image's dimensions.
   * ********************************************************************************/
  void CalculateGridDimensions(unsigned cellWidth,
                               unsigned cellHeight,
                               const cv::Mat& srcImgMat);

  /**********************************************************************************
   * Name: CreateThumbnails
   * Description: creates the thumbnails from the source picture directory.
   * Parameters:
   *    - thumbnailWidth - the width of each thumbnail (same as grid cell width)
   *    - thumbnailHeight - the height of each thumbnail (same as grid cell height)
   *    - normalizeIntensities - a flag indicating whether or not to normalie the
   *        intensities of the thumbnails from 0 to 255. If false, thumbnails will
   *        be mapped with their actual intensity value
   * ********************************************************************************/
  void CreateThumbnails(unsigned thumbnailWidth,
                        unsigned thumbnailHeight,
                        bool normalizeIntensities = true);

  /**********************************************************************************
   * Name: NormalizeIntensities
   * Description: normalizes the intensity of each struct Picture in tnIntensities and
   *    emplaces that new pair in a new map, then returns the map. The new intensities
   *    are normalized between [minIntensity, maxIntensity]
   * Parameters:
   *    - tnIntensities - the map of intensity --> picture data to be normalized
   *    - minIntensity - The minimum intensity value of the normalization
   *    - maxIntensity - the maximum intensity value of the normalization
   * Returns: a map of intensity --> picture data, which has the normalized
   *    intensities
   * ********************************************************************************/
  std::map<double, struct Picture> NormalizeIntensities(const std::map<double, struct Picture>& tnIntensities,
                                                        double minIntensity,
                                                        double maxIntensity);

  /**********************************************************************************
   * Name: StitchMosaic
   * Description: creates the mosaic image
   * Parameters:
   *    - cellWidth - the width of the thumbnail/grid cell
   *    - cellHeight - the height of the thumbnail/grid cell
   * ********************************************************************************/
  void StitchMosaic(unsigned cellWidth,
                    unsigned cellHeight);

  /**********************************************************************************
   * Name: StitchMosaicVideo
   * Description: attemps to open a camera feed and perform mosaic operations on
   *    live video feed
   * Parameters:
   *    - cellWidth - the width of the thumbnail/grid cell
   *    - cellHeight - the height of the thumbnail/grid cell
   * ********************************************************************************/
  void StitchMosaicVideo(unsigned cellWidth,
                         unsigned cellHeight);

  /**********************************************************************************
   * Name: FindMatchingThumbnail
   * Description: find the thumbnail picture whose intensity most closely matches the
   *    the intensity of the current cell (whose intensity is the passed in parameter)
   * Parameters:
   *    - intensity - the intensity that we most closely want to find a match for in
   *        our map (in practice this is the intensity of the current cell)
   * Returns: a reference to the struct Picture in the thumbnail map that most closely
   *    matches the passed in intensity
   * ********************************************************************************/
  const struct Picture& FindMatchingThumbnail(double intensity);

  /**********************************************************************************
   * Name: FeatureGrayScalingNormalization
   * Description: computes Feature Scaling Normalization
   * Parameters:
   *    - minIntensity - the minimum intensity to use in the calculation
   *    - maxIntensity - the max intensity to use in the calculation
   *    - curIntensity - the current intensity that must be normalized
   * Returns: the normalized intensity
   * ********************************************************************************/
  double FeatureGrayScalingNormalization(double minIntensity,
                                         double maxIntensity,
                                         double curIntensity);

  /**********************************************************************************
   * Name: LoadThumbnails
   * Description: this loads the intensity information from thumbnails that have
   *    already been created. It does this by reading a file that stores this
   *    information when the thumbnails are created
   * Parameters:
   *    - cellWidth - on return, contains the cellWidth read from file
   *    - cellHeight - on return, contains the cellHeight read from file
   * ********************************************************************************/
  void LoadThumbnails(unsigned& cellWidth,   // this is read from the file
                      unsigned& cellHeight); // this is read from the file

  /**********************************************************************************
   * Name: GetGrayscaleIntensity
   * Description: calculates the intensity of the passed in curImage. This could be
   *    an entire picture, or a cell of an image
   * Parameters:
   *    - curImage - the cv::Mat we want to calculate the intensity of. This function
   *        doesn't know or care if curImage is representative of an entire picture
   *        or a cell of a larger image
   * Returns: the average intensity of curImage
   * ********************************************************************************/
  double GetGrayscaleIntensity(const cv::Mat& curImage);

  /**********************************************************************************
   * Name: DirectoryExists
   * Description: determines whether or not a directory exists on the system. If it
   *    does not exist and createIfDne is true, it will try to create the directory.
   *    Making this static since it's not really related to creating a mosaic. In a
   *    real project this would probably be grouped in a class with utility functions
   *    or a collection of static utility functions
   * Parameters:
   *    - dir - the file path of the directory to question. This can be a full or
   *        relative path
   *    - createIfDne - if this is true, the function will attempt to create the
   *        directory if it does not exist
   * ********************************************************************************/
  static bool DirectoryExists(const std::string& dir,
                              bool createIfDne);

  // members

  // maps the intensity value of the full picture --> thumbnail struct Picture data
  std::map<double, struct Picture> intensityToTnMap_;
  // height (number of rows) of the grid laid down on srcImage
  unsigned gridHeight_;
  // width (number of cols) of the grid laid down on srcImage
  unsigned gridWidth_;
  // the image we want mosaiced. always required
  cv::String srcImage_;
  // directory of already-exiting thumbnails (if they've already been created)
  std::string tnSrcDir_;
  // destination directory where thumbnails should be saved
  std::string tnDestDir_;
  // the directory of images that must be converted to thumbnails
  std::string srcImagesDir_;
  // the full path of the output mosaic image.
  cv::String outputMosaicImage_;
};

#endif
