
// c headers
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>

// c++ headers
#include <string>
#include <fstream>
#include <iostream>
#include <map>
#include <utility>
#include <set>
#include <algorithm>
#include <functional>
#include <stdexcept>

// local headers
#include "yanoMosaic.h"

//typedef std::function<bool(const std::pair<double, struct Picture>&,
//                           const std::pair<double, struct Picture>&)> FrequencyComparitor;

// Default constructor.. just don't think it makes much sense to have any others. I could
// collect all the inputs I need first, then create the object, but I'm not sure much is
// gained over the current strategy.
YanoMosaic::YanoMosaic()
  : gridHeight_(0)
  , gridWidth_(0)
  // initialize these optional arguments to default values. They will be overridden only if
  // the user specifies. outputMosaicImage_ needs some code to determine its default value
  // and won't be initialized to anything special here
  , tnDestDir_(std::string("./"))
{

}

YanoMosaic::~YanoMosaic()
{

}

void YanoMosaic::SetSourceImage(const cv::String& srcImage)
{
  srcImage_ = srcImage;

  // if the output mosaic filename isn't specified, create its name based on the
  // source image
  if (outputMosaicImage_.length() == 0)
  {
    size_t dotPos = srcImage.find_last_of('.');
    if (dotPos == cv::String::npos)
    {
      // if there's no extesion, simply append
      outputMosaicImage_ = srcImage + cv::String("_mos");
    }
    else
    {
      // if there is an extension, squeeze the suffix in right in
      // before the extension
      outputMosaicImage_ = srcImage.substr(0, dotPos) + cv::String("_mos") + srcImage.substr(dotPos);
    }
  }
}

void YanoMosaic::SetThumbnailDestDir(const std::string& tnDestDir)
{
  tnDestDir_ = AddTrailingSlash(tnDestDir);
}

void YanoMosaic::SetThumbnailSrcDir(const std::string& tnSrcDir)
{
  // has the -p option been specified? -t and -p are mutually exclusive
  if (srcImagesDir_.length() > 0)
  {
    throw std::invalid_argument("-t and -p options are mutually exclusive, only specify one.");
  }
  tnSrcDir_ = AddTrailingSlash(tnSrcDir);
}

void YanoMosaic::SetSourceImagesDir(const std::string& srcImagesDir)
{
  if (tnSrcDir_.length() > 0)
  {
    throw std::invalid_argument("-t and -p options are mutually exclusive, only specify one.");
  }
  srcImagesDir_ = AddTrailingSlash(srcImagesDir);
}

const struct Picture& YanoMosaic::FindMatchingThumbnail(double intensity)
{
  std::map<double, struct Picture>::iterator imIt = intensityToTnMap_.lower_bound(intensity);
  if (imIt == intensityToTnMap_.end())
  {
    // update the usage
    (intensityToTnMap_.rbegin())->second.numTimesUsed++;
    return (intensityToTnMap_.rbegin())->second;
  }
  // update the usage
  imIt->second.numTimesUsed++;

  return imIt->second;
}

void YanoMosaic::ConvertGridDimensions(const std::string &strGridDims)
{
  size_t xPos = strGridDims.find_first_of('x');

  if (xPos < std::string::npos)
  {
    // TODO: error check these arguements
    gridWidth_ = static_cast<unsigned>(strtoul(strGridDims.substr(0, xPos).c_str(), nullptr, 10));
    gridHeight_ = static_cast<unsigned>(strtoul(strGridDims.substr(xPos+1, std::string::npos).c_str(), nullptr, 10));

    std::cout << "Setting grid to " << gridWidth_ << " width x " << gridHeight_ << " height." << std::endl;
  }
  else
  {
    std::ostringstream oss;
    oss << "Invalid argument " << strGridDims << ". Grid dimensions must have an 'x' separating them";
    throw std::invalid_argument(oss.str().c_str());
  }
}

void YanoMosaic::CreateMosaic()
{
  unsigned cellWidth = 0;
  unsigned cellHeight = 0;
  cv::Mat srcImgMat;

  // we must always specify a photo to be mosaiced
  if (srcImage_.length() > 0)
  {
    srcImgMat = cv::imread(srcImage_, cv::IMREAD_GRAYSCALE);
    if (srcImgMat.data)
    {
      std::cout << "Image dimensions are " << srcImgMat.cols << " cols (width) x " << srcImgMat.rows <<
                   " rows (height)." << std::endl;
      std::cout << "Image channels: " << srcImgMat.channels() << std::endl;
      CV_Assert(srcImgMat.depth() == CV_8U);
    }
    else
    {
      std::ostringstream oss;
      oss << srcImage_ << " does not contain image data, it must not be an image format that OpenCV understands.";
      throw std::runtime_error(oss.str().c_str());
    }
  }
  else
  {
    throw std::invalid_argument("You must specify a source image to be mosaiced with the -s option.");
  }

  // are we creating thumbnails or are they already created?
  if (tnSrcDir_.length() > 0)
  {
    // the user has specified to use already-existing thumbnails,
    // there are no other arguments to verify for this option. Load
    // the thumbnails from disc and perform the photomosaic
    LoadThumbnails(cellWidth, cellHeight);
    // calculate the grid dimensions
    CalculateGridDimensions(cellWidth, cellHeight, srcImgMat);
    // Stitch the mosaic together
    StitchMosaic(cellWidth, cellHeight);
  }
  else if (srcImagesDir_.length() > 0)
  {
    // we have a directory of source images that need to be converted to thumbnails
    // For this option, the user must also specify a destination directory where the
    // thumbnails should be saved
    if (tnDestDir_.length() > 0)
    {
      // no more arguments to verify. Convert the source images to thumbnails and
      // perform the photomosaic
      CalculateCellDimensions(cellWidth, cellHeight, srcImgMat);
      // create the thumbnails
      CreateThumbnails(cellWidth, cellHeight);
      // stitch together the final picture
      StitchMosaic(cellWidth, cellHeight);
    }
    else
    {
      throw std::invalid_argument("You must specify a destination directory with the "
                                  "-d option if you are converting images to thumbnails "
                                  "with the -p option.");
    }
  }
  else
  {
    throw std::invalid_argument("You must specify either the -t or -p option.");
  }
}

void YanoMosaic::MosaicLiveVideo()
{
  unsigned cellWidth = 0;
  unsigned cellHeight = 0;

  if (tnSrcDir_.length() > 0)
  {
    // the user has specified a directory of already-existing thumbnails
    // Load the thumbnails from disc
    LoadThumbnails(cellWidth, cellHeight);
    // launch the live video feed
    StitchMosaicVideo(cellWidth, cellHeight);
  }
  else if (srcImagesDir_.length() > 0)
  {
    // we need to convert these to thumbnails
    if (tnDestDir_.length() > 0)
    {
      // launch the live video feed
      StitchMosaicVideo(cellWidth, cellHeight);
    }
    else
    {
      throw std::invalid_argument("You must specify a destination directory with the "
                                  "-d option if you are converting images to thumbnails "
                                  "with the -p option.");
    }
  }
  else
  {
    throw std::invalid_argument("You must specify either the -t or -p option.");
  }
}

void YanoMosaic::CalculateCellDimensions(unsigned& cellWidth,
                                         unsigned& cellHeight,
                                         const cv::Mat& srcImgMat)
{
  if ( (gridHeight_ > 0) &&
       (gridWidth_ > 0) )
  {
    // each cell in the grid is cellWidth x cellHeight pixels
    cellWidth = static_cast<unsigned>(srcImgMat.cols) / gridWidth_;
    cellHeight = static_cast<unsigned>(srcImgMat.rows) / gridHeight_;

    // correct our grid dimensions to fit as much of the picture as possible
    if (static_cast<unsigned>(srcImgMat.cols) % gridWidth_ != 0)
    {
      //widthEven = false;

      std::cout << "Warning, Grid width " << gridWidth_ << " does not evenly divide " <<
                   "the width of the picture (" << srcImgMat.cols << ")." << std::endl;

      // correct gridWidth as much as we can for integer division
      unsigned gridWidthTemp = static_cast<unsigned>(srcImgMat.cols) / cellWidth;

      if (gridWidthTemp != gridWidth_)
      {
        std::cout << "Correcting grid width to " << gridWidthTemp << std::endl;
        gridWidth_ = gridWidthTemp;
      }
    }

    if (static_cast<unsigned>(srcImgMat.rows) % gridHeight_ != 0)
    {
      //heightEven = true;

      std::cout << "Warning, Grid height " << gridHeight_ << " does not evenly divide " <<
                   "the height of the picture (" << srcImgMat.rows << ")." << std::endl;

      unsigned gridHeightTemp = static_cast<unsigned>(srcImgMat.rows) / cellHeight;

      if (gridHeightTemp != gridHeight_)
      {
        std::cout << "Correcting grid height to " << gridHeightTemp << std::endl;
        gridHeight_ = gridHeightTemp;
      }
    }

    std::cout << "final grid laydown is " << gridWidth_ << "x" << gridHeight_ << ", creating "
                 "cells that are " << cellWidth << "x" << cellHeight << ", for a final "
                 "output image of " << gridWidth_*cellWidth << "x" << gridHeight_*cellHeight <<
                 "." << std::endl;
  }
  else
  {
    throw std::invalid_argument("You must specify a grid to laydown on the image using the "
                                "-g option. Each cell of this grid will be replaced by a thumbnail "
                                "whose intensity most closely matches the intensity of the cell on "
                                "the source image. The cell size may be modified slightly to evenly "
                                "divide the dimensions of the picture.");
  }
}

void YanoMosaic::CalculateGridDimensions(unsigned cellWidth,
                                         unsigned cellHeight,
                                         const cv::Mat& srcImgMat)
{
  // the grid is the dimension of the source image integer divided by the cell dimension
  gridWidth_ = static_cast<unsigned>(srcImgMat.cols) / cellWidth;
  gridHeight_ = static_cast<unsigned>(srcImgMat.rows) / cellHeight;
}

void YanoMosaic::CreateThumbnails(unsigned thumbnailWidth,  // also cellWidth
                                  unsigned thumbnailHeight, // also cellHeight
                                  bool normalizeIntensities)
{
  std::string strHighInt, strLowInt;
  double lowestIntensity = 255.0;
  double highestIntensity = 0.0;

  if (DirectoryExists(srcImagesDir_, false))
  {
    DIR* dir;
    struct dirent* ent;

    if ( (dir=opendir(srcImagesDir_.c_str())) != nullptr)
    {
      if (DirectoryExists(tnDestDir_, true))
      {

        std::cout << "Creating thumbnails from " << srcImagesDir_ << "..." << std::endl;
        std::cout << "Writing thumbnails to " << tnDestDir_ << "..." << std::endl;

        std::ofstream outfile (std::string(tnDestDir_ + THUMBNAIL_INTENSITY_FILE),
                               std::ios_base::out |
                               std::ios_base::trunc);
        if (outfile.is_open() == true)
        {
          // first write the cellWidth and cellHeight to the file
          outfile << thumbnailWidth << "x" << thumbnailHeight << std::endl;
        }
        else
        {
          std::cerr << "Unable to open a text file to save the thumbnail instensities. "
                       "You will be unable to reuse these thumbnails in the future, they'll "
                       "have to be created again." << std::endl;
          // this isn't a deal breaker, keep going. The downside is we'll need to check each
          // time if this file is open
        }

        unsigned curPicNum = 1;

        while ((ent=readdir(dir)) != nullptr)
        {
          cv::Mat curImage;
          cv::String tempPicStr = cv::String(srcImagesDir_) + cv::String(ent->d_name);

          // read in as grayscale
          curImage = cv::imread(tempPicStr, cv::IMREAD_GRAYSCALE);
          if (curImage.data)
          {
            struct Picture thumbnail;

            std::cout << "\r" << "Processing image " << curPicNum++ << ": " << ent->d_name <<
                         "                             " << std::flush;
            // save this picture's name
            thumbnail.picName = std::string(ent->d_name);

            double intensity = GetGrayscaleIntensity(curImage);
            if (intensity > highestIntensity)
            {
              highestIntensity = intensity;
              strHighInt = std::string(ent->d_name);
            }
            if (intensity < lowestIntensity)
            {
              lowestIntensity = intensity;
              strLowInt = std::string(ent->d_name);
            }
            // ok, now we have our cell intensity. write it to file so we can use
            // the thumbnails later
            cv::Size newSize(static_cast<int>(thumbnailWidth),
                             static_cast<int>(thumbnailHeight));

            cv::resize(curImage, thumbnail.picData, newSize);

            cv::imwrite(cv::String(tnDestDir_) +
                        cv::String(ent->d_name),
                        thumbnail.picData);

            // insert this into our map. Right now, don't really care if this is a duplicate
            intensityToTnMap_.emplace(intensity, thumbnail);

            // only write these to file here if we're NOT normalizing
            if ( (normalizeIntensities==false) && (outfile.is_open()) )
            {
              outfile << thumbnail.picName << "=" << intensity << std::endl;
            }
          }
        }
        std::cout << std::endl;
        std::cout << "Lowest intensity: " << strLowInt << ", " << lowestIntensity << std::endl;
        std::cout << "Highest intensity: " << strHighInt << ", " << highestIntensity << std::endl;

        // ok, normalize the intensities if specified
        if (normalizeIntensities == true)
        {
          std::map<double, struct Picture>::const_iterator intIt;

          std::cout << "As specified, normalizing intensities... ";
          intensityToTnMap_ = NormalizeIntensities(intensityToTnMap_,
                                                   lowestIntensity,
                                                   highestIntensity);
          std::cout << "Done!" << std::endl;
          std::cout << "Writing Intensities to file... ";
          // write intensities
          for (intIt=intensityToTnMap_.cbegin(); intIt!=intensityToTnMap_.cend(); intIt++)
          {
            outfile << intIt->second.picName << "=" << intIt->first << std::endl;
          }
          std::cout << "Done!" << std::endl;
        }

        if (outfile.is_open())
        {
          outfile.close();
        }
      }
      else
      {
        std::ostringstream oss;

        if (tnDestDir_.length() > 0)
        {
          oss << "Unable to find/create thumbnail destination directory " << tnDestDir_;
        }
        else
        {
          oss << "You must specify an output thumbnail directory (with the -d option) when "
                 "specifying the -p option.";
        }

        throw std::invalid_argument(oss.str().c_str());
      }
    }
    else
    {
      std::ostringstream oss;
      oss << "Unable to open thumbnail directory " << srcImagesDir_;
      throw std::invalid_argument(oss.str().c_str());
    }
  }
  else
  {
    std::ostringstream oss;
    if (srcImagesDir_.length() > 0)
    {
      oss << srcImagesDir_ << " directory does not exist.";
    }
    else
    {
      oss << "You must specify a directory of source images to convert to thumbnails for the "
             "-p option.";
//      oss << "You must specify a destination directory where thumbnails will be saved using "
//             "the -d option whenever you specify creating thumbnails with the -p option.";
    }
    throw std::invalid_argument(oss.str().c_str());
  }
  // were any photos loaded into our map?
  if (intensityToTnMap_.size() == 0)
  {
    std::ostringstream oss;
    oss << "The intensity map contains no photos. Are you sure " << srcImagesDir_ <<
           " points to a directory with photos?";
    throw std::invalid_argument(oss.str());
  }
}

void YanoMosaic::LoadThumbnails(unsigned& cellWidth,
                                unsigned& cellHeight)
{
  std::string strHighInt, strLowInt;
  double lowestIntensity = 255.0;
  double highestIntensity = 0.0;
  std::ifstream infile(tnSrcDir_ + THUMBNAIL_INTENSITY_FILE);

  if (infile.is_open())
  {
    // surprised this isn't a string in c++
    char curLine[256];
    std::string strCurLine;

    infile.getline(curLine, sizeof(curLine));
    // the first line as the grid parameters
    if (infile.good())
    {
      strCurLine = std::string(curLine);
      // split on the x
      size_t xPos = strCurLine.find_first_of('x');
      if (xPos < std::string::npos)
      {
        std::string strCellWidth = strCurLine.substr(0, xPos);
        std::string strCellHeight = strCurLine.substr(xPos+1);

        // TODO: make this conversion safer
        cellWidth = static_cast<unsigned>(std::stoi(strCellWidth));
        cellHeight = static_cast<unsigned>(std::stoi(strCellHeight));

        // ok, loop through the rest of the file
        infile.getline(curLine, sizeof(curLine));
        while (infile.good())
        {
          strCurLine = std::string(curLine);
          // split on the =
          size_t equalPos = strCurLine.find_first_of('=');
          if (equalPos < std::string::npos)
          {
            struct Picture curTn;

            std::string fileName = strCurLine.substr(0, equalPos);
            std::string strIntensity = strCurLine.substr(equalPos+1);

            // load the thumbnail from disk, apply the intensity after converting it to a double
            curTn.picData = cv::imread(cv::String(tnSrcDir_+fileName), cv::IMREAD_GRAYSCALE);
            if (curTn.picData.data)
            {
              // TODO: make this conversion safer
              double intensity = std::stod(strIntensity);

              curTn.picName = fileName;

              if (intensity > highestIntensity)
              {
                highestIntensity = intensity;
                strHighInt = fileName;
              }
              if (intensity < lowestIntensity)
              {
                lowestIntensity = intensity;
                strLowInt = fileName;
              }

              intensityToTnMap_.emplace(intensity, curTn);
            }
          }
          else
          {
            std::cerr << "Malformed line, could not find = sign: " << strCurLine << std::endl;
          }

          infile.getline(curLine, sizeof(curLine));
        }

        std::cout << std::endl;
        std::cout << "Lowest intensity: " << strLowInt << ", " << lowestIntensity << std::endl;
        std::cout << "Highest intensity: " << strHighInt << ", " << highestIntensity << std::endl;

        // no need to normalize intensities here. they are written to file that way
      }
      else
      {
        std::cerr << "Unable to locate cell dimensions." << std::endl;
        // this really shouldn't be a deal breaker, but it is right now.
      }

      infile.close();
    }
    else
    {
      // this should be a more useful error
      throw std::runtime_error("Unable to read current line from file.");
    }
  }
  else
  {
    std::ostringstream oss;
    oss << "Unable to open " << THUMBNAIL_INTENSITY_FILE;
    throw std::invalid_argument(oss.str().c_str());
  }

  // were any photos loaded into our map?
  if (intensityToTnMap_.size() == 0)
  {
    std::ostringstream oss;
    oss << "The intensity map contains no photos. Are you sure " << tnSrcDir_ <<
           " points the directory with thumbnails?";
    throw std::invalid_argument(oss.str());
  }
}

double YanoMosaic::GetGrayscaleIntensity(const cv::Mat &curImage)
{
  // only grays for now
  CV_Assert(curImage.depth() == CV_8U);

  // scan the image to get its average intensity
  int nRows = curImage.rows;
  int nCols = curImage.cols * curImage.channels();

  if (curImage.isContinuous())
  {
    // this will print out a lot for a large picture
    //std::cout << "Cell (" << gridW << ", " << gridH << ") Image data is continuous." << std::endl;
    nCols *= nRows;
    nRows = 1;
  }

  const uchar* p;
  uint64_t intensitySum = 0;
  for (int y=0; y<nRows; y++)
  {
    p = curImage.ptr<uchar>(y);
    for (int z=0; z<nCols; z++)
    {
      //std::cout << (int)(p[j]) << std::endl;
      intensitySum += static_cast<uint64_t>(p[z]);
    }
  }
  double cellAvgIntensity = static_cast<double>(intensitySum) /
      static_cast<double>(nRows * nCols/*curImage.rows * curImage.cols*/);

  return cellAvgIntensity;
}

double YanoMosaic::FeatureGrayScalingNormalization(double minIntensity,
                                                   double maxIntensity,
                                                   double curIntensity)
{
  // this computes feature scaling normalization
  return MAX_PIXEL_INTENSITY * ( (curIntensity-minIntensity)/(maxIntensity-minIntensity) );
}

std::map<double, struct Picture> YanoMosaic::NormalizeIntensities(const std::map<double, struct Picture>& tnIntensities,
                                                                  double minIntensity,
                                                                  double maxIntensity)
{
  std::map<double, struct Picture> newTnInt;
  // iterate through each item in the map and calculate it's normalized intensity
  std::map<double, struct Picture>::const_iterator intIt;
  for (intIt=tnIntensities.cbegin(); intIt!=tnIntensities.cend(); intIt++)
  {
    double normalizedInt = FeatureGrayScalingNormalization(minIntensity,
                                                           maxIntensity,
                                                           intIt->first);
    newTnInt.emplace(normalizedInt, intIt->second);
  }

  return newTnInt;
}

void YanoMosaic::StitchMosaic(unsigned cellWidth,
                              unsigned cellHeight)
{
  // first, open the source image
  cv::Mat img = cv::imread(cv::String(srcImage_, cv::IMREAD_GRAYSCALE));
  if (img.data)
  {
    cv::Mat outputMat;

    // don't think we need to print this again
//    std::cout << "Image dimensions are " << img.cols << " cols (width) x " << img.rows <<
//                 " rows (height)." << std::endl;
//    std::cout << "Image channels: " << img.channels() << std::endl;
    CV_Assert(img.depth() == CV_8U);

    unsigned outputWidth = gridWidth_ * cellWidth;
    unsigned outputHeight = gridHeight_ * cellHeight;

    std::cout<< "Ouput image dimensions are " << outputWidth << " cols (width) x " <<
                outputHeight << " rows (height)." << std::endl;

    std::cout << "Grid dimensions are " << gridWidth_ << " x " << gridHeight_ << ", for "
                 "a total of " << gridWidth_*gridHeight_ << " cells." << std::endl;

    // open the output image
    outputMat = cv::Mat(static_cast<int>(outputHeight),
                        static_cast<int>(outputWidth),
                        CV_8UC1,
                        cv::Scalar(255));

    // now loop through each cell of the source image, calculate its intensity,
    // find the best matching thumbnail, and write that to the same corresponding
    // cell in the output image
    for (unsigned cellWidthIndex=0; cellWidthIndex<gridWidth_; cellWidthIndex++)
    {
      for (unsigned cellHeightIndex=0; cellHeightIndex<gridHeight_; cellHeightIndex++)
      {
        //std::cout << "Picture " << cellWidthIndex*gridWidth + cellHeightIndex << std::endl;

        // get the cell of the source image
        cv::Mat srcImgCell = cv::Mat(img, cv::Rect(static_cast<int>(cellWidthIndex*cellWidth),
                                                   static_cast<int>(cellHeightIndex*cellHeight),
                                                   static_cast<int>(cellWidth),
                                                   static_cast<int>(cellHeight)));
        // get this cell's intensity
        double srcImgCellIntensity = GetGrayscaleIntensity(srcImgCell);

        // get the cell of the output image
        cv::Mat outputImgCell = cv::Mat(outputMat, cv::Rect(static_cast<int>(cellWidthIndex*cellWidth),
                                                            static_cast<int>(cellHeightIndex*cellHeight),
                                                            static_cast<int>(cellWidth),
                                                            static_cast<int>(cellHeight)));

        // now get the most closely matching thumbnail
        //numLoops++;
        const struct Picture& matchingTn = FindMatchingThumbnail(srcImgCellIntensity);
        // and copy that thumbnail to the current cell
        matchingTn.picData.copyTo(outputImgCell);
      }
    }

    // and finally write the output picture to file
    std::cout << "Writing mosaiced image " << outputMosaicImage_ << " to file." << std::endl;
    cv::imwrite(outputMosaicImage_, outputMat);

//    FrequencyComparitor freqComp =
//        [](const std::pair<double, struct Picture>& lhs, const std::pair<double, struct Picture>& rhs)
//        {
//          return lhs.second.numTimesUsed > rhs.second.numTimesUsed;
//        };

//    std::set<std::pair<double, struct Picture>, FrequencyComparitor> freqUsed(
//          tnIntensities.begin(), tnIntensities.end(), freqComp);

//    unsigned totalUses = 0;
//    for (const auto& fcIt : freqUsed)
//    {
//      std::cout << fcIt.second.picName << ": " << fcIt.second.numTimesUsed <<
//                   ": " << fcIt.first << std::endl;
//      totalUses += fcIt.second.numTimesUsed;
//    }

//    std::cout << "Total Uses from set: " << totalUses << std::endl;

//    totalUses = 0;

//    for (const auto& imIt : tnIntensities)
//    {
//      totalUses += imIt.second.numTimesUsed;
//    }

//    std::cout << "Total Uses from map: " << totalUses << std::endl;
  }
  else
  {
    std::ostringstream oss;
    oss << "Unable to read source image " << srcImage_;
    throw std::invalid_argument(oss.str().c_str());
  }
}

void YanoMosaic::StitchMosaicVideo(unsigned cellWidth,
                                   unsigned cellHeight)
{
  cv::VideoCapture cap(0);
  if (cap.isOpened() == false)
  {
    throw std::runtime_error("Unable to open video camera 0 for live feed capture.");
  }

  cv::Mat initFrame;
  cv::namedWindow("yanoMosaicFeed", cv::WINDOW_AUTOSIZE);

  // get an initial frame so we can get the resolution size of the frame
  cap >> initFrame;
  // surely this must have image data since we're pulling the frame from the camera
  if (initFrame.data)
  {
    std::cout << "Image dimensions are " << initFrame.cols << " cols (width) x " << initFrame.rows <<
                 " rows (height)." << std::endl;
    std::cout << "Image channels: " << initFrame.channels() << std::endl;

    if ( (cellWidth == 0) &&
         (cellHeight == 0) )
    {
      // we need to calculate these based on the frame dimensions, then create
      // thumbnails
      // calculate the cell dimensions
      CalculateCellDimensions(cellWidth, cellHeight, initFrame);
      // create the thumbnails
      CreateThumbnails(cellWidth, cellHeight);
    }
    else
    {
      // we already have thumbnails, cellWidth and cellHeight were read from
      // the file
      CalculateGridDimensions(cellWidth, cellHeight, initFrame);
    }

    // get the grid height and weight, even though these won't be used outside
    // of this function
//    gridWidth_ = static_cast<unsigned>(initFrame.cols) / cellWidth;
//    gridHeight_ = static_cast<unsigned>(initFrame.rows) / cellHeight;

    unsigned outputWidth = gridWidth_ * cellWidth;
    unsigned outputHeight = gridHeight_ * cellHeight;

    std::cout << "Output frame size is " << outputWidth << " cols (width) x " << outputHeight <<
                 " rows (height)." << std::endl;

    cv::Mat mosaicedFrame(static_cast<int>(outputWidth),
                          static_cast<int>(outputHeight),
                          CV_8UC1);

    while (true)
    {
      cv::Mat curFrame;
      // get the current frame
      cap >> curFrame;
      // resize the picture so there's no extra edge beyond the thumbnails
      //cv::Size newSize()
      //curFrame.resize()
      // convert the current frame to black and white
      cv::cvtColor(curFrame, mosaicedFrame, cv::COLOR_BGR2GRAY);
      // resize the curFrame, just cutoff the unused part rather than distorting the whole
      // image to fit
      cv::Size curFrameResize(static_cast<int>(outputHeight),
                              static_cast<int>(outputWidth));
      cv::resize(curFrame, curFrame, curFrameResize, 0, 0, cv::INTER_LINEAR);

      // do the thumbnail substitution
      for (unsigned cellWidthIndex=0; cellWidthIndex<gridWidth_; cellWidthIndex++)
      {
        for (unsigned cellHeightIndex=0; cellHeightIndex<gridHeight_; cellHeightIndex++)
        {
          // get the cell of the source image
          cv::Mat imgCell = cv::Mat(mosaicedFrame, cv::Rect(static_cast<int>(cellWidthIndex*cellWidth),
                                                            static_cast<int>(cellHeightIndex*cellHeight),
                                                            static_cast<int>(cellWidth),
                                                            static_cast<int>(cellHeight)));
          // get this cell's intensity
          double srcImgCellIntensity = GetGrayscaleIntensity(imgCell);
          // find the thumbnail that matches this the closest
          const struct Picture& matchingTn = FindMatchingThumbnail(srcImgCellIntensity);
          // copy that thumbnail to the image's cell
          matchingTn.picData.copyTo(imgCell);
        }
      }

      //cv::GaussianBlur(edges, edges, cv::Size(7,7), 1.5, 1.5);
      //cv::Canny(edges, edges, 0, 30, 3);
      cv::imshow("yanoMosaicFeed", mosaicedFrame);
      if (cv::waitKey(30) >= 0)
      {
        break;
      }
    }
  }
  else
  {
    throw std::runtime_error("Frame from camera does not have any image data.");
  }
}

bool YanoMosaic::DirectoryExists(const std::string &dir,
                                 bool createIfDne)
{
  struct stat sb;

  if ( (stat(dir.c_str(), &sb)==0) && (S_ISDIR(sb.st_mode)) )
  {
    // the directory exists
    return true;
  }
  else
  {
    if (createIfDne)
    {
      // the directory does not exist, try to create it
      if (mkdir(dir.c_str(), S_IRWXU |
                             S_IRGRP | S_IXGRP |
                             S_IROTH |
                             S_IXOTH) == 0)
      {
        return true;
      }
      else
      {
        std::ostringstream oss;
        oss << "Unable to create directory " << dir;
        perror(oss.str().c_str());
      }
    }
  }

  return false;
}

std::string YanoMosaic::AddTrailingSlash(const std::string& dir)
{
  if (dir[dir.length()-1] != '/')
  {
    return dir + std::string("/");
  }
  // else
  return dir;
}


















