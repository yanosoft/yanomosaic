
## Intro
This software creates a photomosaic, both for still photos and in a real time video feed. The user provides a libaray
of photos, and the software will convert these to thumbnails as a pre-calculation step.  On subsequent uses, the
thumbnails can/should be reused.  For a still photo transformation, the user must provide the source photo to be
transformed.  Currently, all transformations are grayscale only, adding functionality for color transformations is
planned as future work.  There is undoubtedly better, commercial grade photomosaic software available elsewhere; this
simply began as a personal project for my personal knowledge enrichment.  This software has a command line interface,
there are no plans to add a graphical user interface.

## License
This software is released under the .... whatever license.  I'm not a lawyer.  This software is provided free to use,
modify, and distribute at will.  It is provided AS IS, I am not responsible for any damage that may occur, or any
embarrasment that may ensue from using compromising photos.  This software does not collect, nor receive, any data
to/from the internet.  It opens no ports.  It does access your video camera if the live video feed option is chosen,
but it does not save any of the video data.

## Software Requirements
This software REQUIRES [OpenCV](https://opencv.org/).  I do not know what versions of OpenCV are compatible, but
I imagine anything relatively recently will be fine.  Please refer to OpenCV documentation for information on
installing that software.

It is RECOMMENDED to use CMake to build this software (and OpenCV) from source, but CMake is not required.  This
software includes a CMakeLists.txt file in lieu of a makefile.  As with OpenCV, any "modern" version of CMake
should work, I'm not entirely sure exactly which versions are compatible.  CMake 3 and above should be fine.

## Examples
To build yanoMosaic using CMake:
  :> cd /to/yanoMosaic/dir
  :> mkdir build
  :> cd build
To build the debug version
  :> cmake ..
To build the release version
  :> cmake -DCMAKE_BUILD_TYPE=Release ..
  :> make

For any other build process you're left to your own devices

To use yanoMosaic:

Prints the help menu of all the options.
  :> ./yanoMosaic -h   ==> This outputs the following:
-t [directory] - Specifies the source directory where thumbnails already live ("
                     "created with the -p option). This is mutually exclusive with the -p option."
-p [directory] - Specifies a source directory of images which will be converted "
                     "to thumbnails. All images must be in the top directory, it does not perform a "
                     "recursive search. This is mutually exclusive with the -t option."
-g [WidthxHeight] - Specifies the grid diemsions to be laid down on sourceImage. "
                     "This is required for the -p option and ignored for the -t option. "
                     "Each grid cell is a mosaic tile. Width and Height must both be > 0 and separated "
                     "with 'x'. There must be no spaces in the string, ie 43x84."
-s [sourceImage] - Specifies the source image to be mosaiced."
-z [outputPath] - Specifies the output path for the final mosaiced image. "
                     "This is optional, and if ommited, the mosaiced image will simply be created "
                     "in the directory where the source image lives."
-d [directory] - The output directory where the thumbnails will be saved when "
                     "the -p option is used. If this is unspecified, then it will be set to the "
                     "current directory."
-v Specifies to launch a live video feed rather than mosaic a single image."

Creates a single mosaiced photo from a library of photos. This will create the thumbnails first, which
takes some time
  :> ./yanoMosaic -p /path/to/photo/library -g 100x100 -s /path/to/photo/you/want/mosaiced -d /thumbnails/saved/here
Creates a single mosaiced photo from already-created thumbnails
  :> ./yanoMosaic -t /thumbnails/are/here -s /photo/to/mosaic
Launches a live video feed with already-created thumbnails
  :> ./yanoMosaic -t /thumbnails/are/here -v

Feel free to fork and issue a pull request for bug fixes/features, or
directly get in contact at yano13 <at> gmail <dot> com













